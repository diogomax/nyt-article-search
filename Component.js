sap.ui.define([
	"sap/ui/core/UIComponent"
], function(UIComponent) {
	"use strict";

	return UIComponent.extend("nyt.artigos.Component", {
		
		/*********************************************
		* Replace the line below with your API Key
		**********************************************/
		apiKey: "9579bbd10f3c4b0587d101d0e32d6e6c",
		
		metadata: {
			manifest: "json"
		},
		
		init: function() {
			UIComponent.prototype.init.apply(this, arguments);
			
			var oManifest = this.getManifest();
			var sPath = oManifest["sap.ui5"].models[""].uri;
			
			var oModel = this.getModel();
			oModel.loadData(sPath,
			{
				"api-key": this.apiKey
			});
			
		}
	});
});